package com.wipro.assignment.suraj_gopal.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.wipro.assignment.suraj_gopal.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
